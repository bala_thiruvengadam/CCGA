(function($) {
	$.fn.menuouter = function(options) {
		return this.each(function() {
			var $this = $(this);
			var center;
			var movementOffset;
			var menuCount;
			var containerHeight;
			var animationSpeed = 100;
			var matchNext;
			var matchPrev;
			var tch = 0;
			var tmv = 0;
			alert($this);
			console.log($this);
			$this.bind('click', function(mdown) {
				alert("Mouse down");
				mdown.preventDefault();
				$(document).bind('mousemove', function(mmove) {
					mmove.preventDefault();
					alert ("Mouse Up");
				});
				$(document).bind('mouseup', function(mup) {
					mup.preventDefault();
					alert ("Mouse up");
				});
			});
		});
	};
})(jQuery);


var circlemenu = function() {
    var center;
    var movementOffset;
    var menuCount;
    var containerHeight;
    var animationSpeed = 100;

    /* Swipe is called when touchmove up or down is detected*/
    var swipe = function(direction) {
            if (direction == 'down' && center > 0) {
                center--;
                $('.menu').animate({
                    'top': '+=' + movementOffset
                }, animationSpeed, 'linear');
            } else if (direction == 'up' && center < (menuCount - 1)) {
                center++;
                $('.menu').animate({
                    'top': '-=' + movementOffset
                }, animationSpeed, 'linear');
            }
            $(".itemcenter").removeClass("itemcenter");
            $('.menu li').eq(center).addClass("itemcenter");
            adjust("animate");
    }

     /*Adjusts padding for circular effect when dom is loaded*/
    var adjust = function(option) {
            var time = ((option == "noanimate") ? 0 : animationSpeed);
            $('.menu li').eq(center).find('.iconclass').css("transform", "scale(1)");
            $('.menu li').eq(center).find('.menuText').css("transform", "scale(1)");

            // Loop forward and backward menu items for calulations (calculation done for only visible 4 elements)
            matchNext = $('.menu li').eq(center).next();
            matchPrev = $('.menu li').eq(center).prev();
            var pad = 20;
            var scale = 1;
            var k = 0;
            while ((k++) < 5) {
                pad *= 1.60;
                scale *= 0.8;


                matchNext.animate({
                    'padding-left': pad
                }, time, 'linear');

                matchNext.find('.iconclass').css("transform", "scale(" + scale + ") ");
                matchNext.find('.menuText').css("transform", "scale(" + scale + ") translate(-" + 0.6*pad + "px)");
                matchNext.find('.menuText').css("-webkit-transform", "scale(" + scale + ") translate(-" + 0.6*pad + "px)");


                matchPrev.animate({
                    'padding-left': pad
                }, time, 'linear');
                matchPrev.find('.iconclass').css("transform", "scale(" + scale + ")");
                matchPrev.find('.menuText').css("transform", "scale(" + scale + ") translate(-" + 0.6*pad + "px)");
                matchNext.find('.menuText').css("-webkit-transform", "scale(" + scale + ") translate(-" + 0.6*pad + "px)");


                matchNext = matchNext.next();
                matchPrev = matchPrev.prev();
            }



    }

    /*Dom ready event for initial calucations and positioning*/
    $(document).ready(function() {
        $('.menu li').each(function() {
            $('<span/>', {
                style: 'background-image:url("' + $(this).data("iconlink") + '")',
                class: 'iconclass'
            }).prependTo(this);
        });
        $('.menu li').eq(center).addClass("itemcenter");
        menuCount = $('.menu li').size();
        containerHeight = $(document).outerHeight();
        movementOffset = Math.round((containerHeight * 2) / menuCount);
        $('.menu li').css('height', movementOffset);
        $('.menu').css('top', -(containerHeight * 0.5));
        center = Math.round(menuCount / 2) - 1;
        adjust("noanimate");
    });


    /*Event registration for drag*/
    var supportTouch = $.support.touch,
        scrollEvent = "touchmove scroll",
        touchStartEvent = supportTouch ? "touchstart" : "mousedown",
        touchStopEvent = supportTouch ? "touchend" : "mouseup",
        touchMoveEvent = supportTouch ? "touchmove" : "mousemove";
    var start = {
        act: false
    };
    $(document).bind(touchStartEvent, '.menuouter', function(event) {
        event.preventDefault();
        event.stopPropagation();
        var data = event.originalEvent.touches ?
            event.originalEvent.touches[0] :
            event;
        start = {
            act: true,
            time: (new Date).getTime(),
            x: data.pageX,
            y: data.pageY,
        };
    }).bind(touchMoveEvent, '.menuouter', function(event) {
        event.preventDefault();
        event.stopPropagation();
        var data = event.originalEvent.touches ?
            event.originalEvent.touches[0] :
            event;
        if (start.act) {
            var timeNow = (new Date).getTime();
            var timeDiff = timeNow - start.time;
            var moveX = start.x - data.pageX;
            var moveY = start.y - data.pageY;

            if (Math.abs(moveY) > (movementOffset / 2) && Math.abs(moveX) < 70 && timeDiff > (animationSpeed)) {
                if (moveY < 0 && center > 0) {
                    swipe("down");
                } else if (moveY > 0 && center < (menuCount - 1)) {
                    swipe("up");
                }
                //restart touch tracking after a move
                start.time = (new Date).getTime();
                start.y = data.pageY;
            }
        }
    }).bind(touchStopEvent, '.menuouter', function(event) {
        event.preventDefault();
        start.act = false;
    });
};